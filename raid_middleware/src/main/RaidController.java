package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import cloud.amazon.AmazonS3Handler;
import cloud.box.BoxHandler;
import cloud.dropbox.DropBoxHandler;
import cloud.entry.EntryWrapper;
import cloud.interfaces.ICloudBaseHandler;
import cloud.interfaces.IRaidController;
import cloud.recovery.RecoveryManager;
/*
 * DropBox Account: benutzername = aic@trash-mail.com, pw = tuwienaic14
 * Box Account: benutzername = aic@trash-mail.com, pw = tuwienaic14
 * Amazon Account: see http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html#config-creds
 */
@SuppressWarnings("javadoc")
public class RaidController implements IRaidController {
	
	private static final Logger logger = LogManager.getLogger(RaidController.class.getName());
	
	private DropBoxHandler dropBoxHandler;
  private BoxHandler boxHandler;
  private AmazonS3Handler amazonHandler;
  
  private Path currentPath = Paths.get("");
  private int MODE = 0;
  private final List<ICloudBaseHandler> handlerList = new ArrayList<>();
	private final ExecutorService pool = Executors.newCachedThreadPool();

	public static void main(String[] args) {
		BasicConfigurator.configure();
		LogManager.getRootLogger().setLevel(Level.INFO);
		new RaidController();
	}

	public RaidController() {

		//BOX API: VERY SLOW
		try {
			boxHandler = new BoxHandler(); //auto INIT
			handlerList.add(boxHandler);
		} catch (Exception e) {
			logger.error("Box is not available yet.", e);
		}
		
		//DROPBOX API: LESS SLOWER THAN AMAZON S3
		try {
			dropBoxHandler = new DropBoxHandler(); //auto INIT
			handlerList.add(dropBoxHandler);
		} catch (Exception e) {
			logger.error("Dropbox is not available yet.", e);
		}

		//AMAZON S3 API: FASTEST API
		try {
			amazonHandler = new AmazonS3Handler();
			handlerList.add(amazonHandler);
		} catch (Exception e) {
			logger.error("Amazon is not available yet.", e);
		}
		
		switch (MODE = handlerList.size()) {
			case 0:	logger.error("Currently no cloud storage available.");
			 				return;
			case 1:	logger.info("Currently only ONE cloud storage available.");
							logger.info("Recovery Mode turned OFF");
							logger.info("Available storage: " + handlerList.get(0).getName());
							break;	
			case 2: logger.info("Currently only TWO cloud storages available.");
							logger.info("Recovery Mode turned ON");
							logger.info("Available storages: " + handlerList.get(0).getName() + ", " + handlerList.get(1).getName());
							break;				
			case 3:	logger.info("ALL cloud storages available :)");
							logger.info("Recovery Mode turned ON");
							break;			
			default:return;
		}
		
//		for (ICloudBaseHandler handler : handlerList) {
//			TestHandler.test(handler);
//		}
//		System.exit(0);

		try {
			scanInput();
		} catch (IOException e) {
			logger.error("unexpected error", e.getCause());
		}
	}

	private void scanInput() throws IOException {
		// Test of Controller logic (mirroring (creating and updating), deleting and reading from all storages)				
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
		String line;
		System.out.print("cloud-root/" + currentPath + ">");
		
		try {
			while ((line = br.readLine()) != null) {		
				String[] args = line.split("\\s+");
								
				if(args.length != 0) {
					switch (args[0]) {
						case "create": {
							if(args.length != 2) {
								logger.error("Input error. Type \"help\" for more information.");
								break;
							}
							
							Path sourceFilePath = Paths.get("src", "test", "resources", args[1]);
							String cloudPathString = getCloudPath(args[1]);
							
							try {
								byte[] bytes = Files.readAllBytes(sourceFilePath);								
								createEntry(cloudPathString, bytes);
							} catch (IOException e) {
								logger.error("Error while reading file '" + sourceFilePath + "'");
							}
							break;
						}
						case "update": {
							if(args.length != 2) {
								logger.error("Input error. Type \"help\" for more information.");
								break;
							}
							
							Path sourceFilePath = Paths.get("src", "test", "resources", args[1]);
							String cloudPathString = getCloudPath(args[1]);
							
							try {
								byte[] bytes = Files.readAllBytes(sourceFilePath);								
								updateEntry(cloudPathString, bytes);
							} catch (IOException e) {
								logger.error("Error while reading file '" + sourceFilePath + "'");
							}
							break;
						}
						case "createdir": {
							if(args.length != 2) {
								logger.error("Input error. Type \"help\" for more information.");
								break;
							}
							
							String cloudPathString = getCloudPath(args[1]);
							createEntry(cloudPathString + "/", null);
							break;
						}
						case "read": {
							if(args.length != 2) {
								logger.error("Input error. Type \"help\" for more information.");
								break;
							}
							
							String cloudPathString = getCloudPath(args[1]);
							try {
								// get fastest storage provider MAYBE AMAZON??
								byte[] content = null;
								if (MODE >= 2) {
									RecoveryManager.getInstance().recover(handlerList, cloudPathString);
									content = RecoveryManager.getInstance().getRecoveryConent();
								} else {
									content = handlerList.get(0).read(cloudPathString, null);
								}
								
								if (content != null) {
									Path newFilePath = Paths.get("src", "test", "downloaded", args[1]);
									Files.write(newFilePath, content);
								}
							} catch (Exception e) {
								logger.error("Error while downloading file '" + cloudPathString + "'", e);
							}
							break;
						}
						case "delete": {
							if(args.length != 2) {
								logger.error("Input error. Type \"help\" for more information.");
								break;
							}

							String cloudPathString = getCloudPath(args[1]);
							cloudPathString = args[1].endsWith("/") ? cloudPathString+"/" : cloudPathString;							
							deleteEntry(cloudPathString);
							break;
						}
						case "ls": {
							listDirectory(getCloudPath(null));
							break;
						}
						case "cd": {
							if(args.length != 2) {
								logger.error("Input error. Type \"help\" for more information.");
								break;
							}
							
							if(args[1].equals("..")) {
								if((currentPath = navigateBack()) == null)
									currentPath = Paths.get("");
							}
							else {
								currentPath = navigateTo(args[1]);
							}
							break;
						}
						
						case "exit":
							return;
							
						default: 
							logger.info("Available options: ");
							logger.info(">> " + "create <file>" + "\t" + "Uploads the given file to all storages");
							logger.info(">> " + "createdir <name>" + "\t" + "Creates a new directory with the given name");
							logger.info(">> " + "update <file>" + "\t" + "Replace the given file on all storages");
							logger.info(">> " + "delete <file>" + "\t" + "Deletes the given file from all storages");
							logger.info(">> " + "read <file>" + "\t\t" + "Downloads the given file to bin/resources");
							logger.info(">> " + "ls" + "\t\t\t" + "List all files/folder in the current directory");
							logger.info(">> " + "cd <dir>" + "\t\t" + "Navigate through folders");
							logger.info(">> " + "exit" + "\t\t\t" + "Quit the program");
							break;
					}	
				}
				System.out.print("cloud-root/" + FilenameUtils.separatorsToUnix(currentPath.toString()) + ">");
			}	
		}
		finally {			
			br.close();
			pool.shutdown();
			pool.shutdownNow();
			logger.info("shutdown successful");
		}		
	}
	
	private String getCloudPath(final String name) {
		if(name == null || name.isEmpty())
			return FilenameUtils.separatorsToUnix(currentPath.toString());
		return FilenameUtils.separatorsToUnix(currentPath.resolve(name).toString());
	}

	@Override
	public void createEntry(final String path, final byte[] content) {		
//	ExecutorService listPool = Executors.newFixedThreadPool(handlerList.size());
		Set<Callable<Void>> callables = new HashSet<Callable<Void>>();
		
		for (final ICloudBaseHandler h : handlerList) {
			callables.add(new Callable<Void>() {			
				@Override
				public Void call() {
					try {
						h.create(path, content);
					} 
					catch (Exception e) {
		      	logger.error(h.getName() + ": " + e.getMessage(), e);
		      }	
					return null;
				}
			});
		}
		
		try {
			pool.invokeAll(callables);
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		} 
//		finally {
//			listPool.shutdown();
//			listPool.shutdownNow();
//		}
	}

	@Override
	public void deleteEntry(final String path) {
//		ExecutorService listPool = Executors.newFixedThreadPool(handlerList.size());
		Set<Callable<Void>> callables = new HashSet<Callable<Void>>();
		
		for (final ICloudBaseHandler h : handlerList) {
			callables.add(new Callable<Void>() {			
				@Override
				public Void call() {
					try {
						h.delete(path);
					} 
					catch (Exception e) {
		      	logger.error(h.getName() + ": " + e.getMessage(), e);
		      }	
					return null;
				}
			});
		}
		
		try {
			pool.invokeAll(callables);
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		} 
//		finally {
//			listPool.shutdown();
//			listPool.shutdownNow();
//		}
	}

	@Override
	public void updateEntry(final String path, final byte[] content) {
//		ExecutorService listPool = Executors.newFixedThreadPool(handlerList.size());
		Set<Callable<Void>> callables = new HashSet<Callable<Void>>();
		
		for (final ICloudBaseHandler h : handlerList) {
			callables.add(new Callable<Void>() {			
				@Override
				public Void call() {
					try {
						h.update(path, content);
					} 
					catch (Exception e) {
		      	logger.error(h.getName() + ": " + e.getMessage(), e);
		      }	
					return null;
				}
			});
		}
		
		try {
			pool.invokeAll(callables);
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		} 
//		finally {
//			listPool.shutdown();
//			listPool.shutdownNow();
//		}
	}

	@Override
	public void listDirectory(final String path) {
		//ExecutorService listPool = Executors.newFixedThreadPool(handlerList.size());
		Set<Callable<List<EntryWrapper>>> callables = new HashSet<Callable<List<EntryWrapper>>>();
		
		for (final ICloudBaseHandler h : handlerList) {	
			callables.add(new Callable<List<EntryWrapper>>() {
	      @Override
	      public List<EntryWrapper> call() {
	      	try {
						return h.list(path);
					} catch (Exception e) {
						logger.error(h.getName() + ": " + e.getMessage(), e);
					} finally {
						//logger.info(h.getName() + " finished");
					}
					return null;
	      }
			});
		}
		
		Map<String, List<String>> mergedMap = new HashMap<String, List<String>>();
		try {
			for (Future<List<EntryWrapper>> task: pool.invokeAll(callables)) {	
				try {
					List<EntryWrapper> list = task.get();	    	
					if(list == null || list.isEmpty())
						continue;
					
					for(EntryWrapper e: list)
						mergedMap = addEntryToMap(e.getFilename(), e.getOwner(), mergedMap);	
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
			}
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		} 

		for(Entry<String, List<String>> entry : mergedMap.entrySet()) {
			System.out.println("\t" + entry.getKey() + " (" + entry.getValue() + ")");
		}
	}
	
	private Map<String, List<String>> addEntryToMap(final String key, final String handlerName, Map<String, List<String>> map) {
		if(map.get(key) == null)
			map.put(key, new ArrayList<String>());					
		map.get(key).add(handlerName);
		return map;
	}

	@Override
	public Path navigateTo(String path) {
		return currentPath.resolve(path);
	}

	@Override
	public Path navigateBack() {
		return currentPath.getParent();
	}

	@Override
	public void readEntry(String path, byte[] content) {
		// TODO Auto-generated method stub
		
	}
}

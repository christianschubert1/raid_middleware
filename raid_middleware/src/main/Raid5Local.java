package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

/**
 * FOWARD LAYOUT
 * 
 * Disk 1   |   Disk 2   |  Disk 3
 *---------------------------------
 *   D1     |     D2     |   P1_2
 *---------------------------------
 *	 P3_4   |     D3     |    D4
 *---------------------------------
 *   D5     |   P5_6     |    D6
 *
 * D1...Data Block 1
 * P1_2... Parity Block of D1 and D2
 * 
 * for other layouts see: http://www.z-a-recovery.com/articles/raid5-variations.aspx
 */

public class Raid5Local {
	private List<File> disks; 
	private final static int BLOCK_SIZE = 2; // in bytes
	
	public Raid5Local(File disk1, File disk2, File disk3) {
		disks = new ArrayList<File>(Arrays.asList(disk1, disk2, disk3));
	}	
	
	public void writeFile(File file, boolean update) {
		if (! update) {
			for (File disk : disks)
				disk.delete();
		}
			
		byte[] dataBlock = new byte[BLOCK_SIZE*2];
		int parityPosition = 2;
		try {	
			FileInputStream fis = new FileInputStream(file);

			while (true) {
				// clear buffer
				Arrays.fill(dataBlock, (byte) 0);
				// read 2 blocks from file
				int bytesRead = fis.read(dataBlock,0,BLOCK_SIZE*2);
				if (bytesRead == -1)
					break;
				// write the 2 blocks and parity block to disks
				write2Blocks(dataBlock, parityPosition);
				// set next parity position
				parityPosition = nextParityPosition(parityPosition);
			}
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	// write 2 blocks and parity bit to disks
	private void write2Blocks(byte[] b, int parityPosition) {
		assert (b.length == BLOCK_SIZE*2);
//		System.out.println("b1: " + (int) (b[0] & 0xFF));
//		System.out.println("b2: " + (int) (b[1] & 0xFF));
//		System.out.println("b3: " + (int) (b[2] & 0xFF));
//		System.out.println("b4: " + (int) (b[3] & 0xFF));
		try {
			List<Integer> writingPos = new ArrayList<Integer>(Arrays.asList(0,1,2));
			writingPos.remove(parityPosition);
			
			// split into block size
			byte[] block1 = Arrays.copyOfRange(b, 0, BLOCK_SIZE);
			byte[] block2 = Arrays.copyOfRange(b, BLOCK_SIZE, BLOCK_SIZE*2);
			
			// write first block to disk
			FileOutputStream fos = new FileOutputStream(disks.get(writingPos.get(0)), true);
			fos.write(block1);
			fos.close();
			
			// write second block to another disk
			fos = new FileOutputStream(disks.get(writingPos.get(1)), true);
			fos.write(block2);
			fos.close();
			
			// write parity block to remaining disk
			fos = new FileOutputStream(disks.get(parityPosition), true);
			byte[] parityBlock = computeParityBlock(block1, block2);
			fos.write(parityBlock);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private byte[] computeParityBlock(byte[] block1, byte[] block2) {
		byte[] parityBlock = new byte[block1.length];
		
		for (int i=0; i < block1.length; i++) {
			parityBlock[i] = (byte) (block1[i] ^ block2[i]);
//			System.out.println("block1: " + Integer.toBinaryString(block1[i]));
//			System.out.println("block2: " + Integer.toBinaryString(block2[i]));
//			System.out.println("parity: " + Integer.toBinaryString(parityBlock[i]));
		}
		
		return parityBlock;
	}
	
	public byte[] readFile() {
		int parityPosition = 2;
		List<Integer> writingPosition = new ArrayList<Integer>(Arrays.asList(0,1,2));
		writingPosition.remove(parityPosition);
		
		List<Byte> readedBytes = new ArrayList<Byte>();
		byte[] block1 = new byte[BLOCK_SIZE];
		byte[] block2 = new byte[BLOCK_SIZE];
		byte[] parity = new byte[BLOCK_SIZE];
		
		FileInputStream fis1 = null;
		FileInputStream fis2 = null;
		FileInputStream fis3 = null;		
		try {
			fis1 = new FileInputStream(disks.get(0));
			fis2 = new FileInputStream(disks.get(1));
			fis3 = new FileInputStream(disks.get(2));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		while (true) {
			try {
				int bytesRead = -1;
				if (parityPosition == 0) {
					bytesRead = fis1.read(parity);
					fis2.read(block1);
					fis3.read(block2);
				} else if (parityPosition == 1) {
					bytesRead = fis1.read(block1);
					fis2.read(parity);
					fis3.read(block2);
				} else {
					bytesRead = fis1.read(block1);
					fis2.read(block2);
					fis3.read(parity);
				}
				// reached EOF
				if (bytesRead == -1)
					break;
												
				readedBytes.addAll(Arrays.asList(ArrayUtils.toObject(block1)));
				readedBytes.addAll(Arrays.asList(ArrayUtils.toObject(block2)));
				
				// set next parity position
				parityPosition = nextParityPosition(parityPosition);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			fis1.close();
			fis2.close();
			fis3.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ArrayUtils.toPrimitive(readedBytes.toArray(new Byte[readedBytes.size()]));
	}
	
	private int nextParityPosition(int currentParityPos) {
		// forward layout
		return (currentParityPos + 1) % 3;
	}
	
	public static void main(String[] args) {
		// DISKS
		File disk1 = Paths.get("src","test","resources","disk1").toFile();
		File disk2 = Paths.get("src","test","resources","disk2").toFile();
		File disk3 = Paths.get("src","test","resources","disk3").toFile();
		// TEST FILE
		File testFile = Paths.get("src", "test", "resources", "test.txt").toFile();
		
		Raid5Local r5 = new Raid5Local(disk1, disk2, disk3);
		r5.writeFile(testFile,false);
		
		System.out.println(new String(r5.readFile()));
	}
	
}

package main;
 
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
 
@SuppressWarnings("javadoc")
public class CloudProps {
    
	private static CloudProps INSTANCE;
	
  private static final String filename = "cloud.txt";
  private Properties cProp;
  private File cFile;
  	
	public static CloudProps getInstance() {
		if (INSTANCE == null) 
			INSTANCE = new CloudProps();	
		return INSTANCE;
	}
  
  private CloudProps() {
    FileReader cIn = null;
    
    try {
      cFile = new File(filename);
      cProp = new Properties();
      
      if(cFile.exists())        
        cProp.load(cIn = new FileReader(cFile));
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try { if(cIn != null) cIn.close(); } catch (Exception e) { e.printStackTrace(); }
    }
  }

  public String getDropBoxAccessToken() {
    return cProp.getProperty("dropbox");       
  }
    
  public void setDropBoxAccessToken(final String token) throws IOException {
    cProp.put("dropbox", token);
    saveConfig();
  }
  
  public String getBoxAccessToken() {
    return cProp.getProperty("box");       
  }
    
  public void setBoxAccessToken(final String token) throws IOException {
    cProp.put("box", token);
    saveConfig();
  }
  
  public String getAmazonAccessToken() {
    return cProp.getProperty("amazon");       
  }
    
  public void setAmazonAccessToken(final String token) throws IOException {
    cProp.put("amazon", token);
    saveConfig();
  }
  
  public void saveConfig() throws IOException {
    FileWriter cOut = null;
    try {
      cProp.store(cOut = new FileWriter(cFile), "Store by " + System.getProperty("user.name"));
    }
    finally {
      if(cOut != null)
        cOut.close();
    } 
  }
}
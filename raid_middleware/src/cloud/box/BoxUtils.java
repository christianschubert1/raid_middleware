package cloud.box;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

import org.apache.log4j.Logger;

import cloud.config.Config;

import com.box.boxjavalibv2.BoxClient;
import com.box.boxjavalibv2.dao.BoxCollection;
import com.box.boxjavalibv2.dao.BoxFile;
import com.box.boxjavalibv2.dao.BoxFolder;
import com.box.boxjavalibv2.dao.BoxItem;
import com.box.boxjavalibv2.dao.BoxTypedObject;
import com.box.boxjavalibv2.exceptions.AuthFatalFailureException;
import com.box.boxjavalibv2.exceptions.BoxServerException;
import com.box.restclientv2.exceptions.BoxRestException;

@SuppressWarnings("javadoc")
public class BoxUtils {
	
	private final Logger logger = Logger.getLogger(this.getClass().getName());
	private final BoxClient client;

	public BoxUtils(final BoxClient client) {
		this.client = client;
	}

	public BoxItem getItem(final String path, boolean getParent) throws BoxRestException, BoxServerException, AuthFatalFailureException {
		if(path == null || path.isEmpty())
			return client.getFoldersManager().getFolder(Config.Box.ROOT_FOLDER, null);
		
		Path tmp = Paths.get(path);	
		if(getParent) {
			if(tmp.getParent() == null) {
				return client.getFoldersManager().getFolder(Config.Box.ROOT_FOLDER, null);
			}
			tmp = tmp.getParent();
		}
		
		Iterator<Path> it = tmp.iterator();
		String itemId = Config.Box.ROOT_FOLDER;
		BoxItem item = null;
		
		while(it.hasNext()) {
			if((item = getEntry(itemId, it.next().toString())) == null)
				break;			
			itemId = item.getId();
		}
		if(item != null) {
			if(item instanceof BoxFolder)
				return client.getFoldersManager().getFolder(item.getId(), null);
			else if(item instanceof BoxFile)
				return client.getFilesManager().getFile(item.getId(), null);
		}
		
	//	logger.error("cloud entry not found - " + path);	
		return null;
	}

	private BoxItem getEntry(final String id, final String match) throws BoxRestException, BoxServerException, AuthFatalFailureException {
		BoxCollection collection = client.getFoldersManager().getFolderItems(id, null);

		if(collection != null) {
			for (BoxTypedObject boxTypedObject : collection.getEntries()) {
				if(boxTypedObject instanceof BoxItem) {
					if(((BoxItem) boxTypedObject).getName().equals(match)) {
						return (BoxItem) boxTypedObject;
					}
				}
			}
		} 
		return null;
	}
}

package cloud.box;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

import main.CloudProps;
import cloud.config.Config;

import com.box.boxjavalibv2.BoxClient;
import com.box.boxjavalibv2.BoxConfigBuilder;
import com.box.boxjavalibv2.IBoxConfig;
import com.box.boxjavalibv2.dao.BoxOAuthToken;
import com.box.boxjavalibv2.exceptions.AuthFatalFailureException;
import com.box.boxjavalibv2.exceptions.BoxServerException;
import com.box.boxjavalibv2.jsonparsing.BoxJSONParser;
import com.box.boxjavalibv2.jsonparsing.BoxResourceHub;
import com.box.restclientv2.exceptions.BoxRestException;
@SuppressWarnings("javadoc")
public class BoxAuthenticator {
	
  private final static String authorizeUrl = "https://www.box.com/api/oauth2/authorize?response_type=code&client_id=";

  private BoxAuthenticator() {
  	//never call
  }

  public static BoxClient authenticate() throws AuthFatalFailureException, IOException {
  	final BoxResourceHub hub = new BoxResourceHub();
  	final BoxJSONParser parser = new BoxJSONParser(hub);
  	final IBoxConfig config = (new BoxConfigBuilder()).build();
  	final BoxClient client = new BoxClient(Config.Box.APP_KEY, Config.Box.APP_SECRET, hub, parser, config);
  	final BoxOAuthToken token = getToken(client);
  	client.authenticate(token);
    return client;
  }

  private static BoxOAuthToken getToken(final BoxClient client) throws IOException, AuthFatalFailureException {
  	BoxOAuthToken token = null;
  	
  	try {
  		if((token = getTokenViaRefreshToken(client)) != null) return token;
  		if((token = getNewAuthToken(client)) != null) return token;
  		throw new AuthFatalFailureException("Could not get Token.");
  	} 
  	finally {
  	  if(token != null)
  	    CloudProps.getInstance().setBoxAccessToken(token.getRefreshToken());
  	}
  }

  private static BoxOAuthToken getTokenViaRefreshToken(final BoxClient client) {
  	try {
  		return client.getOAuthManager().refreshOAuth(CloudProps.getInstance().getBoxAccessToken(), Config.Box.APP_KEY, Config.Box.APP_SECRET);
  	} catch(BoxRestException | BoxServerException | AuthFatalFailureException ex){
  		System.err.println(ex.getMessage());
  	}
  	return null;
  }
  
  private static BoxOAuthToken getNewAuthToken(final BoxClient client) {
    try {
      Desktop.getDesktop().browse(java.net.URI.create(authorizeUrl + Config.Box.APP_KEY));
      return client.getOAuthManager().createOAuth(getCode(), Config.Box.APP_KEY, Config.Box.APP_SECRET, Config.Box.REDIRECT_HOST + Config.Box.REDIRECT_PORT);
  	} catch (BoxRestException | BoxServerException |  AuthFatalFailureException | IOException ex) {
  		ex.printStackTrace();
  	}
  	return null;
  }

  private static String getCode() throws IOException {
    final ServerSocket serverSocket = new ServerSocket(Config.Box.REDIRECT_PORT);
    final Socket socket = serverSocket.accept();
    final BufferedReader in = new BufferedReader (new InputStreamReader (socket.getInputStream()));
    while (true) {
      String token = "";
      try {
        final BufferedWriter out = new BufferedWriter (new OutputStreamWriter (socket.getOutputStream()));
        out.write("HTTP/1.1 200 OK\r\n");
        out.write("Content-Type: text/html\r\n");
        out.write("\r\n");
        token = in.readLine();
        System.out.println(token);
        final String match = "code";
        int loc = token.indexOf(match);
        if(loc > 0) {
          int httpstr = token.indexOf("HTTP")-1;
          token = token.substring(token.indexOf(match), httpstr);
          String parts[] = token.split("=");
          token=parts[1];
          System.out.println(token);
          out.write("Authentication process OK!");
        } else {
          out.write("Code not found in the URL!");
        }
        out.close();
        serverSocket.close();
        return token;
      }
      catch (IOException e) {
      	e.printStackTrace();
        System.exit (1);
        break;
      }
    }
    return "";
  }
}
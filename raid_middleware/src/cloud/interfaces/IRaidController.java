package cloud.interfaces;

import java.nio.file.Path;

@SuppressWarnings("javadoc")
public interface IRaidController {
	public void createEntry(final String path, final byte[] content);
	
	public void updateEntry(final String path, final byte[] content);
	
	public void readEntry(final String path, final byte[] content);
	
	public void deleteEntry(final String path);
	
	public void listDirectory(final String path);
	
	public Path navigateTo(final String path);
	
	public Path navigateBack();
}

package cloud.interfaces;

import java.io.InputStream;
import java.util.List;

import cloud.entry.EntryWrapper;

@SuppressWarnings("javadoc")
public interface ICloudBaseHandler {
	
	public String getName();
	
	public void init() throws Exception;
	
	public void create(final String path, final byte[] bytes) throws Exception;
	
	public void delete(final String path) throws Exception;
	
	public byte[] read(final String path, final String rev) throws Exception;
	
	public InputStream startRead(final String path, final String rev) throws Exception;
	
	public void update(final String path, byte[] bytes) throws Exception;
	
	public EntryWrapper getEntryInfo(final String path) throws Exception;
	
	public List<EntryWrapper> list(final String dir) throws Exception;
}

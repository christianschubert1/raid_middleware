package cloud.amazon;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;

public class AmazonS3Authenticator {

	private AmazonS3Authenticator() {
		////never call
	}
	
	/*
   * The ProfileCredentialsProvider will return your [default]
   * credential profile by reading from the credentials file located at
   * (~/.aws/credentials).
   */
	public static AmazonS3Client authenticate() {	
    AWSCredentials credentials = new ProfileCredentialsProvider().getCredentials();;

    AmazonS3Client client = new AmazonS3Client(credentials,  
    		new ClientConfiguration()
    			.withMaxConnections(100)
    			.withConnectionTimeout(120 * 1000)
    			.withMaxErrorRetry(15));
    
    client.setRegion(Region.getRegion(Regions.EU_WEST_1));
    return client;
	}
}

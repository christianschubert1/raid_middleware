package cloud.recovery;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import cloud.interfaces.ICloudBaseHandler;

@SuppressWarnings("javadoc")
public class RecoveryManager {
	
	private static final Logger logger = LogManager.getLogger(RecoveryManager.class.getName());
	private static RecoveryManager INSTANCE;
	private final static int CLOUD_FILE_MISSING_ERROR = -1;
	
	private List<RecoveryContent> cloudFileAvail;
	
	// Singelton
	private RecoveryManager() {
	}
	
	public static RecoveryManager getInstance() {
		if (INSTANCE == null) 
			INSTANCE = new RecoveryManager();
		
		return INSTANCE;
	}
	
	/**
	 * Replaces a faulty replica by a "good" one.
	 * @param faultyHandler The faulty cloud handler.
	 * @param validHandler The valid (= "good") cloud handler.
	 * @param path The path to the file.
	 * @return Returns true if replacement was successfully. Otherwise it returns false.
	 */
	private boolean replaceFaultyReplica(ICloudBaseHandler faultyHandler, RecoveryContent validRecoveryContent, String path) {
		
		logger.info("recovery manager found faulty entry on " + faultyHandler.getName());
//		logger.info("do you want recover this file " + path + " (Y/N)? " + faultyHandler.getName());
//		
//		Scanner scanner = new Scanner(System.in);		
//		try {
//			if(scanner.next().equals("N")) {
//				logger.info("Recovery aborted by user");
//				return false;
//			}
//		} finally {
//			scanner.close();
//		}
		
		logger.info("start recovery..");
				
		try {
			//faultyHandler.delete(path);
			if(faultyHandler.getEntryInfo(path) != null)
				faultyHandler.update(path, validRecoveryContent.getContent());
			else
				faultyHandler.create(path, validRecoveryContent.getContent());
			
			logger.info("recovery finished successful");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("recovery finished with errors");
		return false;
	}
	
	/**
	 * Checks if all cloud handlers share the same file. Compares the hash values of the files content. 
	 * If all have the same value, it does nothing. If one has an incorrect value, 
	 * it restores the content from one of the other two correct cloud services.
	 * @param boxHandler The cloud handler for the Box repository.
	 * @param dropBoxHandler The cloud handler for the DropBox repository.
	 * @param amazonHandler The cloud handler for the Amazon repository.
	 * @param path The path to the file.
	 * @return Returns true if recovery not necessary (all files are the same) or was successfully. 
	 * 		Otherwise returns false when all files differ from each other or are missing. 
	 */
	public boolean recover(List<ICloudBaseHandler> handlerList, String path) {
		cloudFileAvail = new ArrayList<RecoveryContent>();
		List<ICloudBaseHandler> cloudFileMissing = new ArrayList<ICloudBaseHandler>();
		
		boolean success = true;		
		
		for (ICloudBaseHandler handler : handlerList) {
			RecoveryContent recoveryContent = generateCRCChecksum(handler, path);
			if (recoveryContent.getContent() == null)
				cloudFileMissing.add(handler);
			else
				cloudFileAvail.add(recoveryContent);
		}

		if (cloudFileAvail.size() == handlerList.size()) {
			// case 1: all clouds have files -> compare 3 hash values			
			int invalidPosition = -1;
			if (handlerList.size() == 3) {
				invalidPosition = compare3HashCodes(
					cloudFileAvail.get(0).getHash(), 
						cloudFileAvail.get(1).getHash(), 
							cloudFileAvail.get(2).getHash()
							);
			}
			if (invalidPosition >= 0) {
				// case 1.1: two hash values are the same, one differs (= faulty) 
				// -> replace the faulty one by one of the good ones
				ICloudBaseHandler faultyCloud = cloudFileAvail.get(invalidPosition).getHandler();
				int validPos = invalidPosition+1 >= cloudFileAvail.size() ? invalidPosition-1 : invalidPosition +1;
				//long validKey = cloudFileAvail.get(cloudFileAvail.size()-invalidPosition).getHash();
				RecoveryContent validCloud = cloudFileAvail.get(validPos);
				
				success &= replaceFaultyReplica(faultyCloud, validCloud, path);
			} else if (invalidPosition == -2) {
				// case 1.2: all hash values differ -> no way to compromise, to indicate the "good" file
				success = false;
			} else {
				// case 1.3: hash values are all the same -> everything OK, nothing to do
				success = true;
			}
		} else if (cloudFileMissing.size() == 1) {			
			// case 2: one cloud has missing file
			ICloudBaseHandler faultyCloud = cloudFileMissing.get(0);

			if (cloudFileAvail.size() == 1) {
				// case 2.1: only one cloud has file available -> replace faulty by available file (ignore read error from third cloud)
				success &= replaceFaultyReplica(faultyCloud, cloudFileAvail.get(0), path);
			} else if (cloudFileAvail.size() == 2) {		
				// case 2.2: two clouds have files available -> compare 2 hash values
				if (cloudFileAvail.get(0).getHash() == cloudFileAvail.get(1).getHash()) {
					// case 2.2.1: two hash values are the same -> replace faulty by one of the good ones
					success &= replaceFaultyReplica(faultyCloud, cloudFileAvail.get(0), path);
				} else {
					// case 2.2.2: hash values differ from each other -> no way to compromise
					success = false;
				}
			}
		} else if (cloudFileMissing.size() == 2) {
			// case 3: two clouds have missing files 
				if (cloudFileAvail.size() == 1) {
					// case 3.1: one file available -> replace the missing files by the one who is available
					RecoveryContent validCloud = cloudFileAvail.get(0);
					success &= replaceFaultyReplica(cloudFileMissing.get(0), validCloud, path);
					success &= replaceFaultyReplica(cloudFileMissing.get(1), validCloud, path);
				} else {
					// case 3.2: no files available (one cloud has a read error) -> no way to compromise
					success = false;
				}
		} else if (cloudFileMissing.size() == 3) {
			// case 4: no cloud has a file available -> no way to compromise
			success = false;
		} else {
			// case 5: one or two clouds have files available, but at least one read error occurred -> no way to compromise
			// case 6: all clouds had a read error -> no way to compromise
			success = false;
		}
					
		return success;
	}
	
	public byte[] getRecoveryConent() {
		if(cloudFileAvail == null || cloudFileAvail.isEmpty())
			return null;
		return cloudFileAvail.get(cloudFileAvail.size()-1).getContent();
	}
	
	/**
	 * Compares 3 given hash values
	 * @param first
	 * @param second
	 * @param third
	 * @return Returns the position of the not identical hash value. Returns 0 if all values are the same or -1 if all are not the same.
	 */
	private int compare3HashCodes(long first, long second, long third) {
		if (second == first) {
			if (third == second) {
				return -1; // all the same
			}
			return 2; // third invalid
		} else if (second == third) { 
			return 0; // first invalid			
		} else if (first == third) { 
			return 1; // second invalid
		}
		return -2; // all not the same
	}
	
	/**
	 * Generates a CRC 32-bit checksum for a given file, identified by a cloud base handler and the path to the file.
	 * @param cloudBaseHandler A cloud base handler: Amazon, DropBox or Box.
	 * @param path The unique path to the file, including the filename itself.
	 * @return Returns a CRC 32-bit checksum in the range of 0..4294967295. If the file is missing, it returns CLOUD_FILE_MISSING_ERROR. 
	 * 		Otherwise if an read error occurs, it returns CLOUD_READ_ERROR.
	 */
	public RecoveryContent generateCRCChecksum(ICloudBaseHandler cloudBaseHandler, String path) {
		RecoveryContent recoveryContent = new RecoveryContent(cloudBaseHandler);
		
		try {
			recoveryContent.setContent(cloudBaseHandler.read(path, null));
		} catch (Exception e) {
			recoveryContent.setReadError(true);
			logger.warn("could not read entry from " + recoveryContent.getHandler().getName());	
		}
		return recoveryContent;
	}
}

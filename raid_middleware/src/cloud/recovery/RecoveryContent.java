package cloud.recovery;

import java.util.zip.CRC32;

import cloud.interfaces.ICloudBaseHandler;

public class RecoveryContent {	
	private final static int CLOUD_FILE_MISSING_ERROR = -1;
	
	private final ICloudBaseHandler handler;
	private byte[] content;
	private boolean readError;
	
	public RecoveryContent(ICloudBaseHandler handler) {
		this.handler = handler;
		this.setReadError(false);
	}

	public ICloudBaseHandler getHandler() {
		return handler;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public long getHash() {
		if(content == null)
			return CLOUD_FILE_MISSING_ERROR;
		
		CRC32 crc = new CRC32();
		crc.update(content);
		return crc.getValue();
	}

	public boolean isReadError() {
		return readError;
	}

	public void setReadError(boolean readError) {
		this.readError = readError;
	}
}
